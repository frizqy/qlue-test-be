# QLUE - Test Backend

## SQL

Build Schema

```
create table persons(
id integer primary key,
name varchar(200)
);

INSERT INTO persons (id, name) VALUES (1, 'AAAA'),(2, 'BBBB'),(3, 'CCCC'),(4, 'DDDD'),(5, 'EEEE');


create table items(
id integer primary key,
name varchar(200),
price varchar(10)
  );
  
INSERT INTO items (id, name, price) VALUES (1, 'PROD 1', '1000'), (2, 'PROD 2', '2000'), (3, 'PROD 3', '3000');
  
create table transactions(
id integer primary key,
person_id integer,
item_id integer,
total integer,
date timestamp,
FOREIGN KEY(person_id)
REFERENCES persons(id),
FOREIGN KEY(item_id)
REFERENCES items(id)
);

INSERT INTO transactions (id, person_id, item_id, total, date) VALUES 
(1, 1, 1, 1000, (TIMESTAMP '2022-01-01 10:41:35')),
(2, 1, 2, 2000, (TIMESTAMP '2022-03-01 10:41:35')),
(3, 1, 3, 3000, (TIMESTAMP '2022-04-01 10:41:35')),
(4, 1, 3, 3000, (TIMESTAMP '2022-04-01 10:41:35')),
(5, 1, 3, 3000, (TIMESTAMP '2022-04-01 10:41:35')),

(6, 2, 1, 1000, (TIMESTAMP '2022-01-01 10:41:35')),
(7, 2, 2, 2000, (TIMESTAMP '2022-02-01 10:41:35')),
(8, 2, 2, 2000, (TIMESTAMP '2022-02-01 10:41:35')),
(9, 2, 2, 2000, (TIMESTAMP '2022-02-01 10:41:35')),
(10, 2, 2, 2000, (TIMESTAMP '2022-02-01 10:41:35')),
(11, 2, 3, 3000, (TIMESTAMP '2022-03-01 10:41:35')),

(12, 3, 1, 1000, (TIMESTAMP '2022-01-01 10:41:35')),
(13, 3, 2, 2000, (TIMESTAMP '2022-05-01 10:41:35')),
(14, 3, 3, 3000, NOW());
```

1. Get Data By Name

```
SELECT p.name, SUM(t.total) AS total_price
FROM transactions AS t
INNER JOIN persons AS p ON t.person_id = p.id
GROUP BY p.name;
```

2. Get Data By Month Year

```
SELECT
  SUM(dt.total) AS total,
  dt.month,
   dt.year
FROM (
  SELECT
    t.total,
     t.date,
    TO_CHAR(t.date, 'Month') AS month,
    EXTRACT(YEAR FROM t.date) AS year
  FROM transactions AS t
  ORDER BY t.date ASC
) AS dt
GROUP BY dt.month, dt.year
ORDER BY MAX(dt.date);
```

3. Get Data By Variety

```
SELECT
  p.name,
  i.name,
  COUNT(t.item_id)
FROM transactions AS t
INNER JOIN persons AS p ON t.person_id = p.id
INNER JOIN items AS i ON t.item_id = i.id
GROUP BY p.name, i.name
ORDER BY COUNT(t.item_id) DESC;
```

http://sqlfiddle.com/#!17/67930/8


## NodeJS, Python, Golang or PHP => PHP

```
<?php

function getText($input){
    $result = null;

    $fmod2 = fmod($input, 2);
    $fmod3 = fmod($input, 3);

    if ($fmod2 == 0 && $fmod3 == 0) {
        $result = "CIRCLE STAR";
    } else if ($fmod2 == 0) {
        $result = "CIRCLE";
    } else if ($fmod3 == 0) {
        $result = "STAR";
    }

    return $result;
}

?>
```

## Javascript

```
function convert(response) {
    const h = [];
    const d = [];
    if (response.length > 0) {
        for (var key in response[0]) {
            if (!h.includes(key)) {
                h.push(key);
            }
        }
    }
    for (var idx in response) {
        const data = [];

        var element = response[idx]

        for (var key in h) {
            var obj = h[key];
            data.push(response[idx][obj])
        }
        d.push(data);

    }

    const result = {
        h: h,
        d: d
    }
    return result;
}
```

## Node

Maaf. Belum setup node js di local. Baru mulai mengerjakan :pray:

## Algorithmic

```
<?php
function findComponents($components, $idx, $input, $numRemaining)
{
    if ($numRemaining < 0) {
        return;
    }

    if ($numRemaining == 0) {
        foreach ($components as $index => $value) {
            echo $value;
        }
        echo "<br/>";
        return;
    }

    $temp = ($idx == 0) ? 1 : $components[$idx - 1];
    for ($i = $temp; $i < $input; $i++) {
        $components[$idx] = $i;
        findComponents($components, $idx + 1, $input, $numRemaining - $i);
    }
}

function findSumComponents($input)
{
    $components = [];
    findComponents($components, 0, $input, $input);
}
```